﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HWO
{
    public interface ICarControl
    {
        void YourCarReceived(Id yourCar);
        void GameInitReceived(Race race);
        void CarPositionsUpdated(List<CarPosition> carPositions, int tick);
        void TurboAvailable(TurboAvailable turboAvailable);
    }

    public class CarControl : ICarControl
    {
        private string _myCarColor;
        private readonly Logger _log;
        private readonly Action<double, int> _throttleAction;
        private readonly Action<Direction, int> _switchLaneAction;
        private TrackManager _trackManager;
        double _oldAngle;
        private CarPosition _oldPosition;
        private double _oldSpeed;
        private double _guideFlagRelativePos;
        private double _oldAngleSpeed;
        private SpeedLimiter _speedLimiter;
        private readonly Action<int> _turboAction;
        private TurboHandler _turboHandler;
        private bool _sendingAllowed;
        private bool _initialized;
        private Direction _lastSwitchSent = Direction.None;
        private readonly Brains _brains;
        private bool _qualifying;
        private bool _accelerationTestDone;
        private bool _decelerationTestStarted;
        private double _lastThrottle;
        private bool _decelerationTestDone;
        private int _lastLaneIndex;
        private bool _crashed;
        private bool _switchedLanes;
        private SpeedLimit _currentSpeedLimit;
        private double _currentSpeed;
        private double _expectedSpeed;
        private bool _switchingLanes;

        public CarControl(Logger log, Action<double, int> throttleAction, Action<Direction, int> switchLaneAction, Action<int> turboAction)
        {
            _log = log;
            _throttleAction = throttleAction;
            _switchLaneAction = switchLaneAction;
            _turboAction = turboAction;
            _brains = new Brains(_log);
        }

        public void YourCarReceived(Id yourCar)
        {
            _log.Print(string.Format("Setting my car color to {0}", yourCar.color), 1);
            _myCarColor = yourCar.color;
        }

        public void GameInitReceived(Race race)
        {
            if (_initialized)
            {
                // Already initialized at qualifying

                // TODO: Smarter reset function
                _qualifying = false;
                _lastSwitchSent = Direction.None;
                _crashed = false;
                _currentSpeedLimit = null;
                _currentSpeed = 0.0;
                _oldPosition = null;
                _oldAngle = 0.0;
                _oldAngleSpeed = 0.0;
                _oldSpeed = 0.0;
                return;
            }
            _qualifying = true;
            _trackManager = new TrackManager(_log, race.track, _brains);
            _speedLimiter = new SpeedLimiter(_trackManager, _brains, _log);
            _turboHandler = new TurboHandler(_trackManager);
            var myCar = race.cars.Single(car => car.id.color == _myCarColor);
            _guideFlagRelativePos = myCar.dimensions.guideFlagPosition / myCar.dimensions.length;
            _log.Print(string.Format("Guide flag pos: {0}", _guideFlagRelativePos));
            _initialized = true;
        }

        public void CarPositionsUpdated(List<CarPosition> carPositions, int tick)
        {
            var myPosition = MyCarPosition(carPositions);

            if (_lastLaneIndex != myPosition.piecePosition.lane.endLaneIndex)
            {
                // switched lanes, reseting
                _log.Print(string.Format("Switched lane to {0}", _lastSwitchSent));
                _lastSwitchSent = Direction.None;
                _switchingLanes = false;
            }
            
            if (_turboHandler.ShouldUseTurbo(myPosition.piecePosition.pieceIndex))
            {
                _log.Print(string.Format("Piece index is {0} so I should turbo", myPosition.piecePosition.pieceIndex), 0);
                _turboAction(tick);
                _sendingAllowed = false;
            }

            // TODO: Do this only after the first throttle
            if (myPosition.piecePosition.inPieceDistance > 0 && _sendingAllowed)
            {
                var nextSwitchDirection = _trackManager.NextSwitchDirection(myPosition.piecePosition);
                if (nextSwitchDirection != Direction.None && nextSwitchDirection != _lastSwitchSent)
                {
                    _lastSwitchSent = nextSwitchDirection;
                    _switchLaneAction(nextSwitchDirection, tick);
                    _sendingAllowed = false;
                }
            }

            if (_oldPosition != null)
            {
                _currentSpeed = _trackManager.Distance(_oldPosition, myPosition);

                if (_switchedLanes)
                {
                    //_currentSpeed += 2;
                    _switchedLanes = false;
                }

                var probablyHit = false; 
                if (Math.Abs(_currentSpeed - _expectedSpeed) > 0.5)
                {
                    //_log.Print(string.Format("Speed is not expected. Speed is {0} and expected {1}", _currentSpeed, _expectedSpeed));
                    probablyHit = true;
                }

                var acc = _currentSpeed - _oldSpeed;
                var radius = _trackManager.CurrentTrackRadius(myPosition);
                var angleSpeed = myPosition.angle - _oldAngle;
                _log.Print(string.Format("speed and angle: {0}, {1}", _currentSpeed, myPosition.angle), 0);
                _log.Print(string.Format("speed, nacc, aacc, angle, angle, aspeed: {0}, {1}, {2}, {3}, {4}, {5}", _currentSpeed,
                    radius > 0 ? _currentSpeed * _currentSpeed / radius : 0, angleSpeed - _oldAngleSpeed, _trackManager.CurrentTrackAngle(myPosition),
                    myPosition.angle, angleSpeed/*myPosition.piecePosition.inPieceDistance*/), 0);


                CheckForQualifyingTestNeed(tick, _currentSpeed, acc);

                var brakeDistance = CalculateBrakeDistance(_currentSpeed);
                var speedLimits = _speedLimiter.GetSpeedLimits(myPosition.piecePosition, brakeDistance, _lastSwitchSent);
                foreach (var speedLimit in speedLimits)
                {
                    _log.Print(string.Format("speed limit coming: {0}, {1}", speedLimit.GetMaxSpeed(), speedLimit.GetDistance()), 0);
                }

                // TODO: With the information from Brains, we could calculate the exact throttle needed for the last braking tick.
                // Then we could just keep up the exact speed of the speedlimit
                var throttle = IsBrakingNeeded(speedLimits, _currentSpeed, angleSpeed, myPosition.angle);
                if (throttle < 1.0)
                {
                    _log.Print("Braking!", 0);
                    ThrottleWithTurboAdjust(throttle, tick);
                }
                else
                {
                    FullSpeed(tick);
                }

                _currentSpeedLimit = speedLimits.FirstOrDefault(limit => limit.GetDistance() < 0.001);
                _trackManager.CurrentPositionAndAngle(myPosition.piecePosition, myPosition.angle, angleSpeed,
                    _currentSpeedLimit != null && _currentSpeedLimit.GetMaxSpeed() - _currentSpeed < 0.1, probablyHit);

                //_log.Print(string.Format("Speed: {0}, Limit: {1}", _currentSpeed, _currentSpeedLimit != null ? _currentSpeedLimit.GetMaxSpeed(): 0));

                _log.Print(string.Format("{0};{1};{2};{3};{4};{5};{6}", tick, myPosition.angle, angleSpeed, angleSpeed - _oldAngleSpeed,
                    myPosition.piecePosition.inPieceDistance, _currentSpeed, _currentSpeed * _currentSpeed / _trackManager.CurrentTrackRadius(myPosition)), 0);

                _oldSpeed = _currentSpeed;
                _oldAngleSpeed = angleSpeed;
            }
            else
            {
                FullSpeed(tick);
            }

            _expectedSpeed = (_lastThrottle * _turboHandler.TurboFactor() - _currentSpeed * _brains.GetWindResistanceFactor()) / _brains.GetMass() 
                + _currentSpeed;
            _oldAngle = myPosition.angle;
            _oldPosition = myPosition;
            _lastLaneIndex = myPosition.piecePosition.lane.endLaneIndex;
            _sendingAllowed = true;
        }

        private bool SomeOneNear(List<CarPosition> carPositions)
        {
            var myPos = MyCarPosition(carPositions);
            if (myPos != null)
            {
                var carsOnSamePiece =
                    carPositions.Where(x => x.piecePosition.pieceIndex == myPos.piecePosition.pieceIndex
                        && x.piecePosition.lane == myPos.piecePosition.lane).ToList();
                if (carsOnSamePiece.Count > 0)
                {
                    var myDist = myPos.piecePosition.inPieceDistance;
                    if (carsOnSamePiece.Any(x => x.piecePosition.inPieceDistance > myPos.piecePosition.inPieceDistance
                                                 &&
                                                 (x.piecePosition.inPieceDistance - myPos.piecePosition.inPieceDistance) <
                                                 5))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void CheckForQualifyingTestNeed(int tick, double currentSpeed, double acc)
        {
            if (_qualifying && !_accelerationTestDone && currentSpeed > 0)
            {
                _brains.AccelerationAfterFirstThrottle(acc, _lastThrottle);
                _accelerationTestDone = true;
            }
            else if (_qualifying && _sendingAllowed && _accelerationTestDone && !_decelerationTestStarted)
            {
                _decelerationTestStarted = true;
                SendThrottle(0.0, tick);
                _sendingAllowed = false;
            }
            else if (_qualifying && _decelerationTestStarted && !_decelerationTestDone)
            {
                _decelerationTestDone = true;
                _brains.AccelerationAfterZeroThrottle(_oldSpeed, acc);
            }
        }

        private void ThrottleWithTurboAdjust(double throttleValue, int tick)
        {
            SendThrottle(throttleValue/_turboHandler.TurboFactor(), tick);
        }

        private void FullSpeed(int tick)
        {
            SendThrottle(1.0, tick);
        }

        private void SendThrottle(double throttleValue, int tick)
        {
            if (_sendingAllowed)
            {
                _throttleAction(throttleValue, tick);
                _lastThrottle = throttleValue;
            }
        }

        private double IsBrakingNeeded(List<SpeedLimit> speedLimits, double currentSpeed, double angleSpeed, double angle)
        {
            var accelerating = false;
            if (Math.Abs(angle) > _brains.GetCrashAngle() * 0.95)
            {
                _log.Print("Too high angle, braking", 0);
                return 0.0;
            }

            if (Math.Abs(angleSpeed) > 5)
            {
                _log.Print("Too high angle speed, braking", 0);
                return 0.0;
            }

            if (Math.Abs(angle) > _brains.GetCrashAngle()/2 && Math.Abs(angleSpeed) > 2 && SameSign(angle, angleSpeed))
            {
                _log.Print("Too high angle speed and angle, braking", 0);
                return 0.0;
            }

            if (Math.Abs(angle) > _brains.GetCrashAngle() * 0.7 && Math.Abs(angleSpeed) > 1 && SameSign(angle, angleSpeed))
            {
                _log.Print("Too high angle speed and angle, braking", 0);
                return 0.0;
            }

            if (Math.Abs(angle) > _brains.GetCrashAngle() * 0.85 && Math.Abs(angleSpeed) > 1 && SameSign(angle, angleSpeed))
            {
                _log.Print("Too high angle speed and angle, braking (2)", 0);
                return 0.0;
            }

            foreach (var limit in speedLimits)
            {
                if (Math.Max(limit.GetDistance(), 1) < CalculateBrakeDistance(currentSpeed, limit.GetMaxSpeed()))
                {
                    if (limit.GetDistance() < 0.1)
                    {
                        if (Math.Abs(angle) < _brains.GetCrashAngle()*0.7 && !SameSign(angle, angleSpeed) && Math.Abs(angleSpeed) < 3)
                        {
                            _log.Print("Accelerating out of corner", 0);
                            accelerating = true;
                            continue;
                        }
                    }
                    var futureSpeed = currentSpeed - (currentSpeed * _brains.GetWindResistanceFactor()) / _brains.GetMass();
                    return (futureSpeed > limit.GetMaxSpeed()) ? 0.0
                        : Math.Min((limit.GetMaxSpeed() - futureSpeed) * _brains.GetMass(), limit.GetMaxSpeed() * _brains.GetWindResistanceFactor());
                }
            }

            if (accelerating)
            {
                return 1.0;
            }

            var currentLimit = speedLimits.FirstOrDefault(limit => limit.GetDistance() < 1);
            if (currentLimit != null && Math.Abs(currentLimit.GetMaxSpeed() - currentSpeed) < 0.1)
            {
                var limitSpeed = currentLimit.GetMaxSpeed();
                return limitSpeed*_brains.GetWindResistanceFactor();
            }

            return 1.0;
        }

        private bool SameSign(double angle, double angleSpeed)
        {
            return (angle > 0 && angleSpeed > 0) || (angle < 0 && angleSpeed < 0);
        }

        private double CalculateBrakeDistance(double currentSpeed, double desiredSpeed = 1.0)
        {
            var distance = 0.0;
            while (currentSpeed > desiredSpeed)
            {
                distance += currentSpeed;
                currentSpeed -= (currentSpeed * _brains.GetWindResistanceFactor())/_brains.GetMass();
            }
            return distance;
        }

        private CarPosition MyCarPosition(List<CarPosition> carPositions)
        {
            return carPositions.SingleOrDefault(pos => pos.id.color == _myCarColor);
        }

        public void TurboAvailable(TurboAvailable turboAvailable)
        {
            if (!_crashed)
            {
                _turboHandler.TurboAvailable(turboAvailable);
            }
        }

        public void TurboActivated(TurboStart ts)
        {
            if (ts.color == _myCarColor)
            {
                _turboHandler.TurboActivated();
            }
        }

        public void TurboEnded(TurboEnd te)
        {
            if (te.color == _myCarColor)
            {
                _turboHandler.TurboEnded();
            }
        }

        public void Crashed(Crash crash)
        {
            if (crash.color == _myCarColor)
            {
                _log.Print("crashed");
                _brains.Crashed(_currentSpeedLimit != null && _currentSpeedLimit.GetMaxSpeed() < _currentSpeed);
                _turboHandler.TurboNotAvailable();
                _crashed = true;
            }
        }

        public void Spawned(Spawn spawn)
        {
            if (spawn.color == _myCarColor)
            {
                _log.Print("spawned");
                _crashed = false;
            }
        }

        public void DumpLimits()
        {
            _trackManager.DumpLimits();
        }
    }
}
