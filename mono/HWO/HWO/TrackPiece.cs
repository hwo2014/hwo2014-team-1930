﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HWO
{
    public interface ITrackPiece
    {
        double GetLength(int laneDistance);
        double GetAngle();
        double GetRadius(int laneDistance);
        double GetCornerAdjustment();
        void SetCornerAdjustment(double d);
    }

    public class TrackPiece : ITrackPiece
    {
        private readonly double _radius;
        private readonly double _angle;
        private readonly double _length;
        private readonly bool _hasSwitch;
        private readonly TrackPieceType _type;
        private double _cornerAdjustment = 1.0;

        public TrackPiece(double length, bool hasSwitch)
        {
            _length = length;
            _hasSwitch = hasSwitch;
            _type = TrackPieceType.Straight;
        }

        public TrackPiece(double radius, double angle, bool hasSwitch)
        {
            _radius = radius;
            _angle = angle;
            _hasSwitch = hasSwitch;
            _type = TrackPieceType.Corner;
        }

        public double GetLength(int laneDistance)
        {
            return _type == TrackPieceType.Straight ? _length : CalculateLengthOfCorner(laneDistance);
        }

        public double GetAngle()
        {
            return _angle;
        }

        public double GetRadius(int laneDistance)
        {
            if (_type == TrackPieceType.Straight)
            {
                return 0.0;
            }
            var laneDist = _angle > 0 ? -1 * laneDistance : laneDistance;
            return _radius + laneDist;
        }

        public double GetCornerAdjustment()
        {
            return _cornerAdjustment;
        }

        public void SetCornerAdjustment(double d)
        {
            _cornerAdjustment = _cornerAdjustment * d;
        }

        private double CalculateLengthOfCorner(int laneDistance)
        {
            var laneDist = _angle > 0 ? -1*laneDistance : laneDistance;
            return Math.Abs(2 * Math.PI * (_radius + laneDist) * (_angle / 360));
        }

        private enum TrackPieceType
        {
            Straight,
            Corner
        }

        public bool IsStraight()
        {
            return _type == TrackPieceType.Straight;
        }

        public bool HasSwitch()
        {
            return _hasSwitch;
        }
    }
}
