﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HWO
{
    public interface ITurboHandler
    {
        void TurboAvailable(TurboAvailable turboAvailable);
        bool ShouldUseTurbo(int currentPieceIndex);
        void TurboActivated();
        void TurboEnded();
        double TurboFactor();
    }

    public class TurboHandler : ITurboHandler
    {
        private readonly TrackManager _trackManager;
        private bool _turboAvailable;
        private TurboAvailable _turboInfo;
        private bool _turboActive;
        private double _turboDurationTicks;
        private double _turboFactor;

        public TurboHandler(TrackManager trackManager)
        {
            _trackManager = trackManager;
        }

        public void TurboAvailable(TurboAvailable turboAvailable)
        {
            _turboAvailable = true;
            _turboInfo = turboAvailable;
        }

        public bool ShouldUseTurbo(int currentPieceIndex)
        {
            if (_turboAvailable && currentPieceIndex == _trackManager.GetLongestStraightStartIndex() && !_turboActive)
            {
                _turboAvailable = false;
                return true;
            }
            return false;
        }

        public void TurboActivated()
        {
            if (_turboInfo != null)
            {
                _turboFactor = _turboInfo.turboFactor;
                _turboActive = true;
            }
        }

        public void TurboEnded()
        {
            _turboActive = false;
        }

        public double TurboFactor()
        {
            if (_turboActive)
            {
                return _turboFactor;
            }
            else
            {
                return 1.0;
            }
        }

        public void TurboNotAvailable()
        {
            _turboAvailable = false;
        }
    }
}
