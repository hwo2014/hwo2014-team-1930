﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace HWO
{
    public enum Direction
    {
        Left,
        Right,
        None
    }

    public class Id
    {
        public string name;
        public string color;

        public override string ToString()
        {
            return string.Format("Name: {0}, Color: {1}", name, color);
        }
    }

    public class Lane
    {
        public int startLaneIndex;
        public int endLaneIndex;

        public override string ToString()
        {
            return string.Format("StartLaneIndex: {0}, EndLaneIndex: {1}", startLaneIndex, endLaneIndex);
        }
    }

    public class PiecePosition
    {
        public int pieceIndex;
        public double inPieceDistance;
        public Lane lane;
        public int lap;

        public override string ToString()
        {
            return string.Format("PieceIndex: {0}, InPieceDistance: {1}, Lane: {2}, Lap: {3}", pieceIndex, inPieceDistance, lane, lap);
        }
    }

    public class CarPosition
    {
        public Id id;
        public double angle;
        public PiecePosition piecePosition;

        public override string ToString()
        {
            return string.Format("Id: {0}, Angle: {1}, PiecePosition: {2}", id, angle, piecePosition);
        }
    }

    public class LaneInit
    {
        public int distanceFromCenter;
        public int index;
    }

    public class Piece
    {
        public double length;
        [JsonProperty(PropertyName = "switch")]
        public bool canSwitchLanes;

        public int radius;
        public double angle;
    }

    public class Track
    {
        public string id;
        public string name;
        public List<Piece> pieces;
        public List<LaneInit> lanes;
    }

    public class Dimension
    {
        public double length;
        public double width;
        public double guideFlagPosition;
    }

    public class Car
    {
        public Id id;
        public Dimension dimensions;

    }

    public class RaceSession
    {
        public int laps;
        public int maxLapTimeMs;
        public bool quickRace;
    }

    public class Race
    {
        public Track track;
        public List<Car> cars;
        public RaceSession raceSession;
    }

    public class GameInit
    {
        public Race race;
    }

    public class TurboAvailable
    {
        public double turboDurationMilliseconds;
        public double turboDurationTicks;
        public double turboFactor;
    }

    public class Crash
    {
        public string name;
        public string color;
    }

    public class Spawn
    {
        public string name;
        public string color;
    }

    public class TurboStart
    {
        public string name;
        public string color;
    }

    public class TurboEnd
    {
        public string name;
        public string color;
    }
}
