﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HWO
{
    public interface IBrains
    {
        double GetWindResistanceFactor();
        double GetMass();
        void AccelerationAfterFirstThrottle(double acceleration, double lastThrottle);
        void AccelerationAfterZeroThrottle(double speedBeforeZeroThrottle, double acceleration);
    }

    public class Brains : IBrains
    {
        private readonly Logger _log;
        private double _windResistanceFactor = 0.1;
        private double _mass = 5.0;
        private double _maxNormalAcceleration = 0.512;//0.3;
        private double _crashAngle = 60.0;

        public Brains(Logger log)
        {
            _log = log;
        }

        public double GetWindResistanceFactor()
        {
            return _windResistanceFactor;
        }

        public double GetMass()
        {
            return _mass;
        }

        public void AccelerationAfterFirstThrottle(double acceleration, double lastThrottle)
        {
            _mass = lastThrottle / acceleration;
            _log.Print(string.Format("Mass is {0}", _mass));
        }

        public void AccelerationAfterZeroThrottle(double speedBeforeZeroThrottle, double acceleration)
        {
            _windResistanceFactor = (_mass*acceleration)/speedBeforeZeroThrottle * -1;
            _log.Print(string.Format("Resistance is {0}", _windResistanceFactor));
        }

        public double GetMaxNormalAcceleration()
        {
            return _maxNormalAcceleration;
        }

        public void Crashed(bool speeding)
        {
            if (!speeding)
            {
                _maxNormalAcceleration = 0.8*_maxNormalAcceleration;
                _log.Print("Reducing speed", 0);
            }
        }

        public double GetCrashAngle()
        {
            return _crashAngle;
        }
    }
}
