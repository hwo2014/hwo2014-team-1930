﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HWO
{
    public interface ITrackManager
    {
        double Distance(CarPosition previous, CarPosition current);
        double CurrentTrackAngle(CarPosition position);
        double CurrentTrackRadius(CarPosition position);
        int GetLaneDistance(int laneIndex);
        List<TrackPiece> GetTrackPiecesFromPosition(PiecePosition position, double distanceToCheck);
        int GetLongestStraightStartIndex();
        Direction NextSwitchDirection(PiecePosition position);
        //Tuple<double, int> GetLaneDistanceForDistance(PiecePosition position, double distance); // Tuple has distance and lanedistance
        //void CurrentPositionAndAngle(PiecePosition position, double angle, double angleSpeed, bool nearCurrentSpeedLimit, bool probablyHit);
    }

    public class TrackManager : ITrackManager
    {
        private readonly Logger _log;
        private readonly Brains _brains;
        private readonly Dictionary<int, TrackPiece> _trackPieces = new Dictionary<int, TrackPiece>();
        private readonly Dictionary<int, int> _lanes = new Dictionary<int, int>();
        private int _longestStraightStartIndex = -1;
        private readonly Dictionary<int, List<int>> _switches = new Dictionary<int, List<int>>();
        private bool _trackingAngle;
        private readonly HashSet<int> _trackedPieces = new HashSet<int>();
        private double _trackedMaxAngle;
        private double _trackedMaxAngleSpeed;
        private int _trackedTicks;
        private int _trackedTicksNearSpeedLimit;
        private bool _probablyHit;

        public TrackManager(Logger log, Track track, Brains brains)
        {
            _log = log;
            _brains = brains;
            var pieceId = 0;
            foreach (var piece in track.pieces)
            {
                if (piece.length > 0.0)
                {
                    _trackPieces.Add(pieceId, new TrackPiece(piece.length, piece.canSwitchLanes));
                    _log.Print(string.Format("Added a straight with length {0}", _trackPieces[pieceId].GetLength(0)), 0);
                }
                else
                {
                    _trackPieces.Add(pieceId, new TrackPiece(piece.radius, piece.angle, piece.canSwitchLanes));
                    _log.Print(string.Format("Added a corner with length {0} and angle {1}", _trackPieces[pieceId].GetLength(0),
                        piece.angle), 0);
                }
                pieceId++;
            }

            foreach (var laneInit in track.lanes)
            {
                _lanes.Add(laneInit.index, laneInit.distanceFromCenter);
            }

            CheckLongestStraight();

            foreach (var trackPiece in _trackPieces.Where(tp => tp.Value.HasSwitch()))
            {
                var laneOrder = CalculateOptimalLanes(trackPiece);
                _switches.Add(trackPiece.Key, laneOrder);
            }
        }

        private List<int> CalculateOptimalLanes(KeyValuePair<int, TrackPiece> trackPiece)
        {
            var nextSwitch = _trackPieces.FirstOrDefault(tp => tp.Key > trackPiece.Key && tp.Value.HasSwitch());
            if (nextSwitch.Value == null)
            {
                nextSwitch = _trackPieces.First(tp => tp.Value.HasSwitch());
            }

            var allPieces = new List<TrackPiece> {trackPiece.Value};
            if (nextSwitch.Key > trackPiece.Key)
            {
                allPieces.AddRange(_trackPieces.Where(tp => tp.Key > trackPiece.Key && tp.Key <= nextSwitch.Key).Select(tp => tp.Value));
            }
            else
            {
                allPieces.AddRange(_trackPieces.Where(tp => tp.Key > trackPiece.Key).Select(tp => tp.Value));
                allPieces.AddRange(_trackPieces.Where(tp => tp.Key <= nextSwitch.Key).Select(tp => tp.Value));
            }

            var laneScores = new Dictionary<int, double>();
            foreach (var lane in _lanes)
            {
                var score = 0.0;
                foreach (var allPiece in allPieces)
                {
                    if (!allPiece.IsStraight())
                    {
                        // TODO: Use value from the TrackPiece here and recalculate during the race?
                        score += allPiece.GetLength(lane.Value)/Math.Sqrt(_brains.GetMaxNormalAcceleration()*allPiece.GetRadius(lane.Value));
                    }
                }
                laneScores.Add(lane.Key, score);
            }
            return laneScores.OrderBy(ls => ls.Value).Select(ls => ls.Key).ToList();
        }

        private void CheckLongestStraight()
        {
            var longestStraight = 0.0;
            foreach (var key in _trackPieces.Keys)
            {
                if (!_trackPieces[key].IsStraight())
                {
                    continue;
                }

                var curLength = _trackPieces[key].GetLength(0);
                curLength += CalculateStraight(key, 1);
                if (curLength > longestStraight)
                {
                    longestStraight = curLength;
                    _longestStraightStartIndex = key;
                }
            }

            _log.Print(string.Format("The longest straight starts at piece {0}", _longestStraightStartIndex));
        }

        private double CalculateStraight(int trackPieceKey, int keyDiff)
        {
            var key = FixKey(trackPieceKey + keyDiff);

            if (_trackPieces[key].IsStraight())
            {
                return _trackPieces[key].GetLength(0) + CalculateStraight(key, keyDiff);
            }
            else
            {
                return 0;
            }
        }

        private int FixKey(int keyToFix)
        {
            if (keyToFix < _trackPieces.Keys.Min())
            {
                return _trackPieces.Keys.Max();
            }

            if (keyToFix > _trackPieces.Keys.Max())
            {
                return _trackPieces.Keys.Min();
            }

            return keyToFix;
        }

        public double Distance(CarPosition previous, CarPosition current)
        {
            var previousPieceIndex = previous.piecePosition.pieceIndex;
            var currentPieceIndex = current.piecePosition.pieceIndex;
            if (previousPieceIndex == currentPieceIndex)
            {
                return current.piecePosition.inPieceDistance - previous.piecePosition.inPieceDistance;
            }

            var previousPiece = _trackPieces[previousPieceIndex];

            var laneDistance = _lanes[previous.piecePosition.lane.endLaneIndex];
            var previousPieceLength = previousPiece.GetLength(laneDistance);
            var previousInPiecePosition = previous.piecePosition.inPieceDistance;
            var currentInPiecePosition = current.piecePosition.inPieceDistance;
            var distance = previousPieceLength - previousInPiecePosition + currentInPiecePosition;

            if (current.piecePosition.lane.startLaneIndex == current.piecePosition.lane.endLaneIndex
                && previous.piecePosition.lane.startLaneIndex != previous.piecePosition.lane.endLaneIndex)
            {
                if (previousPiece.IsStraight())
                {
                    //var length = previousPiece.GetLength(laneDistance);
                    //var lanediff = _lanes.Count > 1 ? _lanes[1] - _lanes[0] : 10;
                    //var extra = Math.Sqrt(length*length + lanediff*lanediff) - length;
                    distance += 2.05;
                }
                else
                {
                    var length = previousPiece.GetLength(_lanes[previous.piecePosition.lane.startLaneIndex]);
                    //var total = previousPieceLength/2 + length/2;
                    distance += 1.46 * (previousPieceLength > length ? -1 : 1); //total - previousPieceLength;
                }
            }
            return distance;
        }

        public double CurrentTrackAngle(CarPosition position)
        {
            return _trackPieces[position.piecePosition.pieceIndex].GetAngle();
        }

        public double CurrentTrackRadius(CarPosition position)
        {
            return _trackPieces[position.piecePosition.pieceIndex].GetRadius(_lanes[position.piecePosition.lane.endLaneIndex]);
        }

        public int GetLaneDistance(int laneIndex)
        {
            return _lanes[laneIndex];
        }

        public List<TrackPiece> GetTrackPiecesFromPosition(PiecePosition position, double distanceToCheck)
        {
            var currentPiece = _trackPieces[position.pieceIndex];
            var laneDistance = _lanes[position.lane.endLaneIndex];
            var distanceLeft = distanceToCheck - (currentPiece.GetLength(laneDistance) - position.inPieceDistance);
            var returnList = new List<TrackPiece> { currentPiece };
            var index = position.pieceIndex;
            while (distanceLeft > 0)
            {
                index++;
                if (index >= _trackPieces.Count)
                {
                    index = 0;
                }
                var piece = _trackPieces[index];
                distanceLeft -= piece.GetLength(laneDistance);
                returnList.Add(piece);
            }
            return returnList;
        }

        public int GetLongestStraightStartIndex()
        {
            return _longestStraightStartIndex;
        }

        public Direction NextSwitchDirection(PiecePosition position)
        {
            if (_switches.Count == 0)
            {
                return Direction.None;
            }

            var nextSwitch = _switches.FirstOrDefault(s => s.Key > position.pieceIndex);
            if (nextSwitch.Value == null)
            {
                nextSwitch = _switches.FirstOrDefault();
            }

            if (position.lane.endLaneIndex != nextSwitch.Value.First())
            {
                return nextSwitch.Value.First() > position.lane.endLaneIndex ? Direction.Right : Direction.Left;
            }

            return Direction.None;
        }

        public void CurrentPositionAndAngle(PiecePosition position, double angle, double angleSpeed, bool nearCurrentSpeedLimit, 
            bool probablyHit, bool speedLimitChanged)
        {
            if (_trackingAngle)
            {
                UpdateAngleTracking(position, angle, angleSpeed, nearCurrentSpeedLimit, probablyHit, speedLimitChanged);
            }
            else if (!_trackPieces[position.pieceIndex].IsStraight())
            {
                StartAngleTracking(position, angle, angleSpeed);
            }
        }

        private void UpdateAngleTracking(PiecePosition position, double angle, double angleSpeed, bool nearCurrentSpeedLimit, 
            bool probablyHit, bool speedLimitChanged)
        {
            _trackedTicks++;
            if (probablyHit)
            {
                _probablyHit = true;
            }

            if (nearCurrentSpeedLimit)
            {
                _trackedTicksNearSpeedLimit++;
            }

            if (_trackPieces[position.pieceIndex].IsStraight() )//|| speedLimitChanged)
            {
                _trackingAngle = false;
                var closeEnough = (double)_trackedTicksNearSpeedLimit / _trackedTicks > 0.8;
                if (!closeEnough)
                {
                    _log.Print("Not raising speed limit because speed was not high enough");
                }
                else if (_probablyHit)
                {
                    _log.Print("Not raising speed limit because probably hit someone");
                }
                else if (Math.Abs(_trackedMaxAngle) < _brains.GetCrashAngle() * 0.85 && Math.Abs(_trackedMaxAngleSpeed) < 3)
                {
                    double angleDiff = 60 - Math.Abs(_trackedMaxAngle);
                    var coeff = (angleDiff*angleDiff)/12000 + 1;
                    //_log.Print(string.Format("Raising max acc by {0} for pieces: {1}", coeff, string.Join(", ", _trackedPieces)));
                    _log.Print(string.Format("Raising max acc by {0} for pieces: {1} - {2}", coeff, _trackedPieces.First(), _trackedPieces.Last()));
                    foreach (var trackedPiece in _trackedPieces)
                    {
                        _trackPieces[trackedPiece].SetCornerAdjustment(coeff);
                    }
                }
                else
                {
                    _log.Print("Not raising speed because angle too high");
                }
                ResetTracking();
                //if (speedLimitChanged)
                //{
                //    StartAngleTracking(position, angle, angleSpeed);
                //}
                return;
            }

            _trackedPieces.Add(position.pieceIndex);
            if (Math.Abs(angle) > Math.Abs(_trackedMaxAngle))
            {
                _trackedMaxAngle = angle;
            }

            if (Math.Abs(angleSpeed) > Math.Abs(_trackedMaxAngleSpeed))
            {
                _trackedMaxAngleSpeed = angleSpeed;
            }
        }

        private void StartAngleTracking(PiecePosition position, double angle, double angleSpeed)
        {
            _trackingAngle = true;
            _trackedPieces.Add(position.pieceIndex);
            _trackedMaxAngle = angle;
            _trackedMaxAngleSpeed = angleSpeed;
            _trackedTicks = 0;
            _trackedTicksNearSpeedLimit = 0;
        }

        private void ResetTracking()
        {
            _trackedMaxAngle = 0.0;
            _trackedMaxAngleSpeed = 0.0;
            _trackedTicks = 0;
            _trackedTicksNearSpeedLimit = 0;
            _probablyHit = false;
            _trackedPieces.Clear();
        }

        public void DumpLimits()
        {
            //_log.Print(string.Format("Global max norm acc: {0}", _brains.GetMaxNormalAcceleration()));
            //foreach (var trackPiece in _trackPieces.Where(tp => !tp.Value.IsStraight()))
            //{
            //    _log.Print(string.Format("Piece number {0}, coeff {1}", trackPiece.Key, trackPiece.Value.GetCornerAdjustment()));
            //}
        }
    }
}
