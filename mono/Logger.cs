﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HWO
{
    public class Logger
    {
        private readonly int _threshold;

        public Logger(int threshold = int.MaxValue)
        {
            _threshold = threshold;
        }

        public void Print(string toPrint, int level = int.MaxValue)
        {
            if (level >= _threshold)
            {
                Console.WriteLine(toPrint);
            }
        }
    }
}
