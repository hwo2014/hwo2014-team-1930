﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HWO
{
    public class SpeedLimit
    {
        private readonly double _distance;
        private readonly double _maxSpeed;

        public SpeedLimit(double distance, double maxSpeed)
        {
            _distance = distance;
            _maxSpeed = maxSpeed;
        }

        public double GetDistance()
        {
            return _distance;
        }

        public double GetMaxSpeed()
        {
            return _maxSpeed;
        }
    }

    public interface ISpeedLimiter
    {
        List<SpeedLimit> GetSpeedLimits(PiecePosition position, double distanceToCheck, Direction lastSwitchSent);
    }

    public class SpeedLimiter : ISpeedLimiter
    {
        private readonly TrackManager _trackManager;
        private readonly Brains _brains;
        private readonly Logger _log;

        public SpeedLimiter(TrackManager trackManager, Brains brains, Logger log)
        {
            _trackManager = trackManager;
            _brains = brains;
            _log = log;
        }

        public List<SpeedLimit> GetSpeedLimits(PiecePosition position, double distanceToCheck, Direction lastSwitchSent)
        {
            var laneDistance = _trackManager.GetLaneDistance(position.lane.endLaneIndex);
            var trackPieces = _trackManager.GetTrackPiecesFromPosition(position, distanceToCheck);
            var speedLimits = new List<SpeedLimit>();
            var distance = 0.0;
            var firstHandled = false;
            var lastMaxSpeed = -1.0;
            foreach (var trackPiece in trackPieces)
            {
                if (firstHandled && trackPiece.HasSwitch() && lastSwitchSent != Direction.None)
                {
                    var old = laneDistance;
                    laneDistance = _trackManager.GetLaneDistance(position.lane.endLaneIndex + (lastSwitchSent == Direction.Left ? -1 : 1));
                    lastSwitchSent = Direction.None;
                    _log.Print(string.Format("Changing lane distance from {0} to {1}", old, laneDistance), 0);
                }

                var radius = trackPiece.GetRadius(laneDistance);
                if (radius > 0)
                {
                    var maxSpeed = Math.Sqrt(_brains.GetMaxNormalAcceleration() * radius * trackPiece.GetCornerAdjustment());
                    if (Math.Abs(maxSpeed - lastMaxSpeed) > 0.1)
                    {
                        speedLimits.Add(new SpeedLimit(distance, maxSpeed));
                    }
                    lastMaxSpeed = maxSpeed;
                }

                if (!firstHandled)
                {
                    firstHandled = true;
                    distance += trackPiece.GetLength(laneDistance) - position.inPieceDistance;
                }
                else
                {
                    distance += trackPiece.GetLength(laneDistance);
                }
            }
            return speedLimits;
        }
    }
}
