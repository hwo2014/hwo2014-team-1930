using System;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace HWO
{
    public class Bot
    {
        private readonly StreamWriter _writer;
        private readonly Logger _logger;

        private Bot(StreamReader reader, StreamWriter writer, Join @join, Logger logger, CreateRace create)
        {
            _writer = writer;
            _logger = logger;
            string line;
            var carControl = new CarControl(_logger, ThrottleAction, SwitchLaneAction, TurboAction);

            //Send(create);
            Send(join);

            while ((line = reader.ReadLine()) != null)
            {
                var msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                switch (msg.msgType)
                {
                    case "carPositions":
                        var positions = JsonConvert.DeserializeObject<List<CarPosition>>(msg.data.ToString());
                        _logger.Print(string.Format("Car positions received, {0}", msg.gameTick), 0);
                        carControl.CarPositionsUpdated(positions, msg.gameTick);
                        break;
                    case "join":
                        _logger.Print(string.Format("Joined, {0}", msg.gameTick));
                        Send(new Ping(msg.gameTick));
                        break;
                    case "gameInit":
                        _logger.Print(string.Format("Race init, {0}", msg.gameTick));
                        var gameInit = JsonConvert.DeserializeObject<GameInit>(msg.data.ToString());
                        carControl.GameInitReceived(gameInit.race);
                        Send(new Ping(msg.gameTick));
                        break;
                    case "gameEnd":
                        _logger.Print(string.Format("Race ended, {0}", msg.gameTick));
                        carControl.DumpLimits();
                        Send(new Ping(msg.gameTick));
                        break;
                    case "gameStart":
                        _logger.Print(string.Format("Race starts, {0}", msg.gameTick));
                        Send(new Ping(msg.gameTick));
                        break;
                    case "yourCar":
                        _logger.Print(string.Format("Your car received, {0}", msg.gameTick));
                        var id = JsonConvert.DeserializeObject<Id>(msg.data.ToString());
                        carControl.YourCarReceived(id);
                        Send(new Ping(msg.gameTick));
                        break;
                    case "crash":
                        _logger.Print(string.Format("Crash received, {0}", msg.gameTick));
                        var crash = JsonConvert.DeserializeObject<Crash>(msg.data.ToString());
                        carControl.Crashed(crash);
                        //Send(new Ping(msg.gameTick));
                        break;
                    case "spawn":
                        _logger.Print(string.Format("Spawn received, {0}", msg.gameTick));
                        var spawn = JsonConvert.DeserializeObject<Spawn>(msg.data.ToString());
                        carControl.Spawned(spawn);
                        //Send(new Ping(msg.gameTick));
                        break;
                    case "lapFinished":
                        _logger.Print(string.Format("Lap finished received, {0}", msg.gameTick));
                        //Send(new Ping(msg.gameTick));
                        break;
                    case "turboAvailable":
                        _logger.Print(string.Format("Turbo available received, {0}", msg.gameTick));
                        carControl.TurboAvailable(JsonConvert.DeserializeObject<TurboAvailable>(msg.data.ToString()));
                        break;
                    case "turboStart":
                        _logger.Print(string.Format("Turbo start received, {0}", msg.gameTick));
                        carControl.TurboActivated(JsonConvert.DeserializeObject<TurboStart>(msg.data.ToString()));
                        break;
                    case "turboEnd":
                        _logger.Print(string.Format("Turbo end received, {0}", msg.gameTick));
                        carControl.TurboEnded(JsonConvert.DeserializeObject<TurboEnd>(msg.data.ToString()));
                        break;
                    case "dnf":
                    case "finish":
                    default:
                        _logger.Print(string.Format("Received {1}, not responding, {0}", msg.gameTick, msg.msgType));
                        //Send(new Ping(msg.gameTick));
                        break;
                }
            }
        }

        private void SwitchLaneAction(Direction direction, int tick)
        {
            Send(new SwitchLane(direction, tick));
        }

        private void ThrottleAction(double value, int tick)
        {
            Send(new Throttle(value, tick));
        }

        private void TurboAction(int tick)
        {
            Send(new TurboActivate(tick));
        }

        public static void Main(string[] args)
        {
            var host = args[0];
            var port = int.Parse(args[1]);
            var botName = args[2];
            var botKey = args[3];
            var logger = args.Length > 4 ? new Logger(Convert.ToInt32(args[4])) : new Logger();

            logger.Print("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

            using (var client = new TcpClient(host, port))
            {
                var stream = client.GetStream();
                var reader = new StreamReader(stream);
                var writer = new StreamWriter(stream) {AutoFlush = true};

                var id = new BotId {name = botName, key = botKey};
                var trackName = args.Length > 5 ? args[5] : "usa";
                var create = new CreateRace(id, trackName);

                new Bot(reader, writer, new Join(botName, botKey), logger, create);
            }
        }

        private void Send(SendMsg msg)
        {
            _logger.Print(string.Format("Sending {0} with value {1} and tick {2}", msg.MsgType(), msg.MsgData(), msg.MsgTick()), 0);
            _writer.WriteLine(msg.ToJson());
        }
    }

    internal class MsgWrapper
    {
        public Object data;
        public string msgType;
        public string gameId;
        public int gameTick;

        public MsgWrapper(string msgType, Object data, int gameTick)
        {
            this.msgType = msgType;
            this.data = data;
            this.gameTick = gameTick;
        }
    }

    internal abstract class SendMsg
    {
        protected int Gametick;

        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapper(MsgType(), MsgData(), MsgTick()));
        }
        
        public virtual Object MsgData()
        {
            return this;
        }

        public abstract string MsgType();

        public virtual int MsgTick()
        {
            return Gametick;
        }
    }

    internal class Join : SendMsg
    {
        public string key;
        public string name;

        public Join(string name, string key)
        {
            this.name = name;
            this.key = key;
        }

        public override string MsgType()
        {
            return "join";
        }
    }

    internal class JoinRace : SendMsg
    {
        public BotId botId;
        public string trackName;
        public string password;
        public int carCount;

        public JoinRace(string name, string key)
        {
            botId = new BotId();
            botId.key = key;
            botId.name = name;
            carCount = 4;
        }

        public override string MsgType()
        {
            return "joinRace";
        }
    }

    internal class BotId
    {
        public string name;
        public string key;
    }

    internal class CreateRace : SendMsg
    {
        public BotId botId;
        public string trackName;
        public string password;
        public int carCount;

        public CreateRace(BotId id, string trackName)
        {
            botId = id;
            this.trackName = trackName;
            carCount = 1;
        }

        public override string MsgType()
        {
            return "createRace";
        }
    }

    internal class Ping : SendMsg
    {
        public Ping(int gameTick)
        {
            this.Gametick = gameTick;
        }

        public override string MsgType()
        {
            return "ping";
        }
    }

    internal class Throttle : SendMsg
    {
        private readonly double _value;

        public Throttle(double value, int gameTick)
        {
            this._value = value;
            this.Gametick = gameTick;
        }

        public override Object MsgData()
        {
            return _value;
        }

        public override string MsgType()
        {
            return "throttle";
        }
    }

    internal class SwitchLane : SendMsg
    {
        private readonly string _direction; 

        public SwitchLane(Direction direction, int gameTick)
        {
            _direction = direction.ToString();
            this.Gametick = gameTick;
        }

        public override object MsgData()
        {
            return _direction;
        }

        public override string MsgType()
        {
            return "switchLane";
        }
    }

    internal class TurboActivate : SendMsg
    {
        public TurboActivate(int gameTick)
        {
            this.Gametick = gameTick;
        }

        public override object MsgData()
        {
            return "I am turbomies!";
        }

        public override string MsgType()
        {
            return "turbo";
        }
    }
}